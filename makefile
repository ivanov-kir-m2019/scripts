YEAR=2019
WORKDIR=pw$(YEAR)
FILE=repos.lst
GITURL="https://<nickname>:<password>@bitbucket.org/"
TASK?=task1
print:
	@while read -r file; do \
            echo "$$file"; \
        done < $(FILE)
clone:
	@while read -r file; do \
	    git clone $(GITURL)"$$file"/$(TASK).git $(WORKDIR)/"$$file"/$(TASK); \
            echo ">>>>>>>>>>>>>>>>>>>>>>>"; \
	done < $(FILE)
pull:
	@while read -r file; do \
            cd $(WORKDIR)/"$$file"/$(TASK) && git pull origin master && cd ../../..; \
            echo ">>>>>>>>>>>>>>>>>>>>>>>"; \
        done < $(FILE)
ls:
	@while read -r file; do \
            echo $(WORKDIR)/"$$file"/$(TASK); \
            ls -lah $(WORKDIR)/"$$file"/$(TASK); \
            echo ">>>>>>>>>>>>>>>>>>>>>>>"; \
        done < $(FILE)
log:
	@while read -r file; do \
            cd $(WORKDIR)/"$$file"/$(TASK) && git log && cd ../../..; \
            echo ">>>>>>>>>>>>>>>>>>>>>>>"; \
        done < $(FILE)
